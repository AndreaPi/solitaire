﻿using System;
using UnityEngine;



[Serializable]
public class CardManager : MonoBehaviour
{

    // INSPECTOR
#pragma warning disable CS0649
    [SerializeField] private Sprite cardBack;
    [SerializeField] private float movementSpeed = 0.1f;
    [SerializeField] private AudioClip audioClip;
#pragma warning restore CS0649

    // VARIABLES
    [SerializeField] [HideInInspector] private Animator animator;
    [SerializeField] [HideInInspector] private AudioSource audioSource;

    [SerializeField] [HideInInspector] private Sprite cardFront;
    [SerializeField] [HideInInspector] private int score;
    [SerializeField] [HideInInspector] private int intValue;
    [SerializeField] [HideInInspector] private bool isRedSeed;
    [SerializeField] [HideInInspector] private GameManager gameManager;

    [SerializeField] [HideInInspector] private bool hasFaceUp;    
    [SerializeField] [HideInInspector] private Vector3 targetMovePoint;
    [SerializeField] [HideInInspector] private bool isMoving = false;
    [SerializeField] [HideInInspector] private Color defaultColor;



    private void Awake()
    {
        this.animator = this.gameObject.GetComponent<Animator>();
        this.defaultColor = this.GetComponent<SpriteRenderer>().color;
    }



    // MANAGE CARD MOVEMENT
    private void Update()
    {
        if (this.isMoving)
        {
            if (Vector3.Distance(this.transform.position, this.targetMovePoint) > this.movementSpeed * Time.deltaTime)
            {
                this.transform.position = Vector3.MoveTowards(this.transform.position, this.targetMovePoint, this.movementSpeed * Time.deltaTime);
            }
            else
            {
                this.isMoving = false;
                this.transform.position = this.targetMovePoint;                
            }
        }
    }



    // MUST BE CALLED RIGHT AFTER INSTANTIATE
    public void Init(Sprite cardFront, char seed, char value, GameManager gameManager)
    {
        this.cardFront = cardFront;
        this.name = seed.ToString() + value.ToString();
        this.isRedSeed = Constants.IsRedSeed(seed);
        this.score = Constants.GetScore(value);
        this.intValue = Constants.GetValue(value);
        this.gameManager = gameManager;
        this.audioSource = gameObject.AddComponent<AudioSource>();
    }



    // CHECK IF CARD IS TURNED FACE UP OR DOWN
    public bool IsTurnedFaceUp()
    {
        return this.hasFaceUp;
    }



    // GETTERS / SETTER
    public int GetScore()
    {
        return this.score;
    }

    public int GetValue()
    {
        return this.intValue;
    }

    public char GetSeed()
    {
        return this.name[0];
    }

    public Color GetDefaultColor()
    {
        return this.defaultColor;
    }

    public Vector3 GetTargetPosition()
    {
        return this.targetMovePoint;
    }



    // START THE MOVEMENT
    public void MoveTo(Vector3 targetPoint)
    {
        this.audioSource.PlayOneShot(this.audioClip);
        this.isMoving = true;
        this.targetMovePoint = targetPoint;        
        this.transform.position = new Vector3(
            this.transform.position.x,
            this.transform.position.y,
            gameManager.IsDeck(this.transform.parent.gameObject) ? this.transform.position.z : -0.3f
            );
    }

    public void JumpTo(Vector3 targetPoint)
    {
        this.audioSource.PlayOneShot(this.audioClip);
        this.targetMovePoint = this.transform.position = targetPoint;
    }



    // TURN CARD FACE UP/DOWN
    public void TurnFaceUp(bool hasFaceUp)
    {
        this.audioSource.PlayOneShot(this.audioClip);
        this.hasFaceUp = hasFaceUp;
        this.animator.SetTrigger("Flip");
    }



    // TURN FLIP FROM ANIMATOR
    public void FlipSprite()
    {
        if (hasFaceUp)
        {
            this.GetComponent<SpriteRenderer>().sprite = this.cardFront;
        }
        else
        {
            this.GetComponent<SpriteRenderer>().sprite = this.cardBack;
        }
    }



    // CHECK IF 2 CARDS HAS OPPOSITE COLOURS SEED
    public static bool AreOppositeSeed(GameObject card1, GameObject card2)
    {
        return Constants.IsRedSeed(card1.GetComponent<CardManager>().GetSeed()) != Constants.IsRedSeed(card2.GetComponent<CardManager>().GetSeed());
    }



    // CONVERT POSITION WHILE CHANGING SCREEN ORIENTATION
    public void ConvertToOrientation(bool isPortrait)
    {
        if(this.transform.parent != null && this.isMoving)
        {
            if (isPortrait && gameManager.IsDeck(this.transform.parent.gameObject))
            {
                this.CalculateNewPosition(Constants.POSITION_DECK_LANDSCAPE, Constants.POSITION_DECK_PORTRAIT);
            }
            if (!isPortrait && gameManager.IsDeck(this.transform.parent.gameObject))
            {
                this.CalculateNewPosition(Constants.POSITION_DECK_PORTRAIT, Constants.POSITION_DECK_LANDSCAPE);
            }

            if (isPortrait && gameManager.IsTableau(this.transform.parent.gameObject))
            {
                this.CalculateNewPosition(Constants.POSITION_TABLEAUS_LANDSCAPE, Constants.POSITION_TABLEAUS_PORTRAIT);
            }
            if (!isPortrait && gameManager.IsTableau(this.transform.parent.gameObject))
            {
                this.CalculateNewPosition(Constants.POSITION_TABLEAUS_PORTRAIT, Constants.POSITION_TABLEAUS_LANDSCAPE);
            }

            if (isPortrait && gameManager.IsFoundation(this.transform.parent.gameObject))
            {
                this.CalculateNewPosition(Constants.POSITION_FOUNDATIONS_LANDSCAPE, Constants.POSITION_FOUNDATIONS_PORTRAIT);
            }
            if (!isPortrait && gameManager.IsFoundation(this.transform.parent.gameObject))
            {
                this.CalculateNewPosition(Constants.POSITION_FOUNDATIONS_PORTRAIT, Constants.POSITION_FOUNDATIONS_LANDSCAPE);
            }
        }
    }

    private void CalculateNewPosition(Vector3 originPilePosition, Vector3 newPilePosition)
    {
        float x = this.targetMovePoint.x - originPilePosition.x + newPilePosition.x;
        float y = this.targetMovePoint.y - originPilePosition.y + newPilePosition.y;
        float z = this.targetMovePoint.z - originPilePosition.x + newPilePosition.z;
        this.targetMovePoint = new Vector3(x, y, z);
    }
}
