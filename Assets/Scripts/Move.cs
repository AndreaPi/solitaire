﻿using System.Collections.Generic;
using UnityEngine;



public class Move
{
    public GameObject originPile;
    public GameObject targetPile;
    public List<GameObject> cards;
    public int deckId;



    public Move(GameObject originPile, GameObject targetPile, List<GameObject> cards)
    {
        this.originPile = originPile;
        this.targetPile = targetPile;
        this.cards = cards;
        this.deckId = -1;
    }

    public Move(GameObject originPile, GameObject targetPile, GameObject card) : this(originPile, targetPile, card, -1) { }

    public Move(GameObject originPile, GameObject targetPile, GameObject card, int deckId)
    {
        this.originPile = originPile;
        this.targetPile = targetPile;
        this.deckId = deckId;
        this.cards = new List<GameObject>();
        cards.Add(card);
    }

}
