﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



[Serializable]
public class GameManager : MonoBehaviour
{

    // INSPECTOR
#pragma warning disable CS0649
    [SerializeField] private Sprite[] cardFronts;
    [SerializeField] private GameObject cardPrefab;

    [SerializeField] private GameObject deck;
    [SerializeField] private GameObject[] foundations;
    [SerializeField] private GameObject[] tableaus;

    [SerializeField] private float wasteOffset;
    [SerializeField] private float wasteCardOffset;

    [SerializeField] private Text textButtonHomePortrait;
    [SerializeField] private Text textErrorPortrait;
    [SerializeField] private Text textPausePortrait;
    [SerializeField] private Text textScorePortrait;

    [SerializeField] private Text textButtonHomeLandscape;
    [SerializeField] private Text textErrorLandscape;
    [SerializeField] private Text textPauseLandscape;
    [SerializeField] private Text textScoreLandscape;
    
    [SerializeField] private Button hintsPortrait;
    [SerializeField] private Button hintsLandscape;

    [SerializeField] private GameObject panelPausePortrait;
    [SerializeField] private GameObject panelPauseLandscape;

    [SerializeField] private GameObject particleHearts;
    [SerializeField] private GameObject particleDiamonds;
    [SerializeField] private GameObject particleSpades;
    [SerializeField] private GameObject particleFlowers;
    [SerializeField] private Text textVictoryPortrait;
    [SerializeField] private Text textVictoryLandscape;
#pragma warning restore CS0649

    // VARIABLES
    [SerializeField] [HideInInspector] private GameObject selectedCard;
    [SerializeField] [HideInInspector] private List<GameObject> tips;
    [SerializeField] [HideInInspector] private int score;
    [SerializeField] [HideInInspector] private bool tipsUp = false;
    [SerializeField] [HideInInspector] private Move lastMove;
    [SerializeField] [HideInInspector] private float[] offsetFaceUpDeckCards;



    // INITIALIZE
    private void Awake()
    {
        this.tips = new List<GameObject>();
        this.textButtonHomePortrait.text = this.textButtonHomeLandscape.text = TextManager.GetTextButtonHome();
        this.textErrorPortrait.text = this.textErrorLandscape.text = TextManager.GetTextError();
        this.textPausePortrait.text = this.textPauseLandscape.text = TextManager.GetTextPause();
        this.textScorePortrait.text = this.textScoreLandscape.text = TextManager.GetTextScore() + this.score;
        this.textVictoryPortrait.text = this.textVictoryLandscape.text = TextManager.GetTextVictory();

        offsetFaceUpDeckCards = new float[GameModel.GetDifficulty()];
        for (int i = 0; i < GameModel.GetDifficulty(); i++)
        {
            offsetFaceUpDeckCards[i] = wasteOffset + i * wasteCardOffset;
        }
    
        this.GenerateDeck();
    }



    // START
    private void Start()
    {
        this.PrepareCards();
    }



    // MANAGE PLAYER INPUT
    private void Update()
    {
        this.ManageMouse();
    }



    // GENERATE THE DECK AND SHUFFLE IT
    private void GenerateDeck()
    {
        List<GameObject> newCards = new List<GameObject>();
        int i = 0;
        foreach (char seed in Constants.SEEDS)
        {
            foreach (char value in Constants.VALUES)
            {
                GameObject newCard = Instantiate(this.cardPrefab, new Vector3(this.deck.transform.position.x, this.deck.transform.position.y, this.deck.transform.position.z), Quaternion.identity);
                newCard.GetComponent<CardManager>().Init(this.cardFronts[i], seed, value, this);
                i++;

                newCards.Add(newCard);
            }
        }

        List<GameObject> shuffledCards = Constants.Shuffle(newCards);
        foreach (GameObject card in shuffledCards)
        {
            this.AddCardToPile(card, this.deck);
        }
    }



    // SET UP THE CARDS ON THE TABLE
    private void PrepareCards()
    {
        float delay = 0f;
        for (int i = 0; i < this.tableaus.Length; i++)
        {
            for (int j = 0; j <= i; j++)
            {
                StartCoroutine(this.PrepareCard(delay, this.tableaus[i], i==j));
                delay += Constants.TIME_PREPARE_DELAY;
            }

        }
    }

    IEnumerator PrepareCard(float delay, GameObject pile, bool mustTurn)
    {
        yield return new WaitForSeconds(delay);
        this.AddCardToPile(GameManager.GetTopCard(this.deck), pile);
        if (mustTurn)
            GameManager.GetTopCard(pile).GetComponent<CardManager>().TurnFaceUp(true);
    }



    // MANAGE INPUTS
    private void ManageMouse()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject()) { return; }
            Vector3 mousePosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit)
            {
                // click on empty deck
                if (hit.transform.gameObject == this.deck)
                {
                    if (this.selectedCard != null)
                        if (this.selectedCard.transform.parent == this.deck.transform)
                            this.DeselectCard(this.selectedCard);
                    this.AdjustDeck();
                    this.AddScore(Constants.POINTS_DECK_RESHUFFLE);
                    return;
                }
                // click on deck
                if (hit.transform.parent.gameObject == this.deck && !hit.transform.gameObject.GetComponent<CardManager>().IsTurnedFaceUp())
                {
                    this.lastMove = null;
                    if (this.selectedCard != null)
                        if (this.selectedCard.transform.parent == this.deck.transform)
                            this.DeselectCard(this.selectedCard);
                    foreach (float offset in this.offsetFaceUpDeckCards)
                    {
                        GameObject topCardFaceDown = GameManager.GetTopFaceUpCard(this.deck, false);
                        if (topCardFaceDown == null)
                        {
                            this.AdjustWaste();
                            return;
                        }
                        GameObject topCardFaceUp = GameManager.GetTopFaceUpCard(this.deck, true);
                        float z;
                        if (topCardFaceUp == null)
                            z = this.deck.transform.position.z + Constants.OFFSET_Z;
                        else
                            z = topCardFaceUp.GetComponent<CardManager>().GetTargetPosition().z + Constants.OFFSET_Z;
                        topCardFaceDown.GetComponent<CardManager>().MoveTo(new Vector3(this.deck.transform.position.x + offset, this.deck.transform.position.y, z));
                        topCardFaceDown.GetComponent<CardManager>().TurnFaceUp(true);
                    }
                    return;
                }
                // click on into waste
                if (hit.transform.parent.gameObject == this.deck && GameManager.GetTopFaceUpCard(this.deck, true) == hit.transform.gameObject)
                {
                    this.SelectDeselectCard(hit.transform.gameObject);
                    return;
                }
                // tableau
                foreach (GameObject tableau in this.tableaus)
                {
                    // click empty tableau
                    if (hit.transform.gameObject == tableau && this.selectedCard != null)
                    {
                        this.lastMove = this.AddFlushToPile(this.selectedCard, tableau);
                        return;
                    }
                    // click on face up card into tableau
                    if (hit.transform.parent.gameObject == tableau && hit.transform.gameObject.GetComponent<CardManager>().IsTurnedFaceUp())
                    {
                        if (this.selectedCard == null)
                        {
                            this.SelectCard(hit.transform.gameObject);
                            return;
                        }
                        if (this.selectedCard.transform.parent == hit.transform.parent)
                        {
                            this.DeselectCard(hit.transform.gameObject);
                            return;
                        }
                        if (this.selectedCard != null && this.selectedCard.transform.parent != tableau.transform)
                        {
                            this.lastMove = this.AddFlushToPile(this.selectedCard, tableau);
                            return;
                        }
                    }
                    // click face down card into tableau
                    if (hit.transform.parent.gameObject == tableau &&
                        GameManager.GetTopCard(tableau) == hit.transform.gameObject &&
                        !GameManager.GetTopCard(tableau).GetComponent<CardManager>().IsTurnedFaceUp())
                    {
                        hit.transform.gameObject.GetComponent<CardManager>().TurnFaceUp(true);
                        this.lastMove = null;
                        return;
                    }
                }
                // foundation        
                foreach (GameObject foundation in this.foundations)
                {
                    if (hit.transform.gameObject == foundation || hit.transform.parent.gameObject == foundation)
                    {
                        if (this.selectedCard != null && this.selectedCard == GameManager.GetTopFaceUpCard(this.selectedCard.transform.parent.gameObject, true))
                        {
                            this.lastMove = this.AddFlushToPile(this.selectedCard, foundation);
                        }
                        return;
                    }
                }
            }
        }
    }



    // ADD A CARD TO PILE, ADJUST ALL DETAILS AND RETURN THE MOVE DONE
    private Move AddCardToPile(GameObject card, GameObject pile)
    {
        return this.AddCardToPile(card, pile, false);
    }

    private Move AddCardToPile(GameObject card, GameObject pile, bool force)
    {
        if (!this.IsValidMove(card, pile) && !force) 
        { 
            return null;
        }
        Move move = null;
        if(card.transform.parent != null)
            if(card.transform.parent == this.deck.transform)
                move = new Move(card.transform.parent.gameObject, pile, card, card.transform.GetSiblingIndex());
            else
                move = new Move(card.transform.parent.gameObject, pile, card);

        float x = pile.transform.position.x;
        float y;
        float z;
        // card created now
        if (GameManager.GetTopCard(pile) == null || card.transform.parent == null)
        {            
            y = pile.transform.position.y;
            z = pile.transform.position.z + Constants.OFFSET_Z;
        }
        else
        {
            // back to waste
            if(card.transform.parent != this.deck && pile == this.deck)
            {
                GameObject topWasteCard = GameManager.GetTopFaceUpCard(pile, true);
                x = this.deck.transform.position.x + this.offsetFaceUpDeckCards[this.offsetFaceUpDeckCards.Length - 1];
                y = this.deck.transform.position.y;
                z = GameManager.GetTopFaceUpCard(pile, true).GetComponent<CardManager>().GetTargetPosition().z + Constants.OFFSET_Z;
            }
            else // all others movements
            {                
                if (GameManager.GetTopCard(pile).GetComponent<CardManager>().IsTurnedFaceUp())
                    y = GameManager.GetTopCard(pile).GetComponent<CardManager>().GetTargetPosition().y + pile.GetComponent<PileManager>().offsetYFaceUp;
                else
                    y = GameManager.GetTopCard(pile).GetComponent<CardManager>().GetTargetPosition().y + pile.GetComponent<PileManager>().offsetYFaceDown;
                z = GameManager.GetTopCard(pile).GetComponent<CardManager>().GetTargetPosition().z + Constants.OFFSET_Z;
            }
        }

        GameObject originPile;
        if (card.transform.parent == null)
            originPile = null;
        else
            originPile= card.transform.parent.gameObject;
            
        card.transform.parent = pile.transform;

        if (this.IsTableau(originPile))
            card.GetComponent<CardManager>().MoveTo(new Vector3(x, y, z));        
        else
            card.GetComponent<CardManager>().MoveTo(new Vector3(x, y, z));

        if (
            (originPile == this.deck && (this.IsTableau(pile) || this.IsFoundation(pile))) ||
            ((this.IsTableau(originPile) || this.IsFoundation(originPile)) && pile == this.deck)
            )
            this.AdjustWaste();
            

        if (this.IsFoundation(pile))
        {
            this.AddScore(card.GetComponent<CardManager>().GetScore());
            this.CheckIfVictory();
        }
        this.DeselectCard(card);
        return move;
    }



    // ADD ALL CARDS UNDER THE SELECTED ONE AND RETURN THE 1ST MOVE
    private Move AddFlushToPile(GameObject card, GameObject pile)
    {
        List<GameObject> flush = GameManager.GetTopFlush(card);

        Move move = new Move(card.transform.parent.gameObject, pile, flush);

        foreach (GameObject flushCard in flush)
        {
            if (this.AddCardToPile(flushCard, pile, false) == null)
                return null;
        }

        return move;
    }



    // CHECK IF THE MOVE IS VALID
    private bool IsValidMove(GameObject card, GameObject pile)
    {
        if (pile == this.deck)
            return true;

        if (this.IsTableau(pile))
        {
            // for creation
            if (!card.GetComponent<CardManager>().IsTurnedFaceUp())
                return true;
            // for player movement
            GameObject topPileCard = GameManager.GetTopCard(pile);
            if (topPileCard == null)
                return true;
            if (
                CardManager.AreOppositeSeed(card, topPileCard) &&
                card.GetComponent<CardManager>().GetValue() == topPileCard.GetComponent<CardManager>().GetValue() - 1
                )
                return true;
        }

        if (this.IsFoundation(pile))
        {
            // check score
            int pileValue;
            GameObject topCard = GameManager.GetTopCard(pile);
            if (topCard == null)
                pileValue = 0;
            else
                pileValue = topCard.GetComponent<CardManager>().GetValue();
            if (card.GetComponent<CardManager>().GetValue() == pileValue + 1)
            {
                // check seed
                for(int i=0; i<foundations.Length; i++)
                {
                    if(pile == foundations[i])
                    {
                        if (card.GetComponent<CardManager>().GetSeed() == Constants.SEEDS[i])
                        {                            
                            return true;
                        }                            
                        else
                            return false;
                    }
                }                
            }
        }

        return false;
    }



    // UPDATE SCORE
    private void AddScore(int score)
    {

        this.score += score;
        this.textScorePortrait.text = this.textScoreLandscape.text = TextManager.GetTextScore() + this.score;
    }



    // ADJUST CARDS POSITION WHEN REBUILDING THE DECK
    private void AdjustDeck()
    {
        for (int i = 0; i < this.deck.transform.childCount; i++)
        {
            GameObject card = this.deck.transform.GetChild(i).gameObject;
            float x = this.deck.transform.position.x;
            float y = i * this.deck.GetComponent<PileManager>().offsetYFaceDown + this.deck.transform.position.y;
            float z = (i + 1) * Constants.OFFSET_Z;
            card.GetComponent<CardManager>().MoveTo(new Vector3(x, y, z));
            card.GetComponent<CardManager>().TurnFaceUp(false);
        }
    }



    // ADJUST CARDS POSITIONS WHEN CHANGING THE WASTE
    public void AdjustWaste()
    {
        if (this.offsetFaceUpDeckCards.Length == 1) return;
        // create waste 
        List<GameObject> waste = new List<GameObject>();
        foreach (Transform card in this.deck.transform)
        {
            if (card.gameObject.GetComponent<CardManager>().IsTurnedFaceUp())
            {
                waste.Add(card.gameObject);
            }
        }
        // move cards from waste to an array that represents positions
        GameObject[] positions = new GameObject[this.offsetFaceUpDeckCards.Length];
        while (positions[positions.Length-1] == null && waste.Count > 0)
        {
            for(int i=positions.Length-1; i>0; i--)
            {
                positions[i] = positions[i - 1];
            }
            positions[0] = GameManager.PullTopCard(waste);
        }
        // adjust cards from positions + waste
        for(int i=0; i<positions.Length; i++)
        {
            if (positions[i] == null)
                return;
            Vector3 newPosition = new Vector3(
                this.deck.transform.position.x + this.offsetFaceUpDeckCards[i],
                this.deck.transform.position.y,
                positions[i].GetComponent<CardManager>().GetTargetPosition().z
                );
            positions[i].GetComponent<CardManager>().MoveTo(newPosition);
        }
        foreach(GameObject card in waste)
        {
            Vector3 newPosition = new Vector3(
                this.deck.transform.position.x + this.offsetFaceUpDeckCards[0],
                card.GetComponent<CardManager>().GetTargetPosition().y,
                card.GetComponent<CardManager>().GetTargetPosition().z
                );
            card.GetComponent<CardManager>().JumpTo(newPosition);
        }
    }



    // CHECK IF A PILE IS A TABLEAU
    public bool IsDeck(GameObject pile)
    {
        if (pile == this.deck)
                return true;
        return false;
    }



    // CHECK IF A PILE IS A TABLEAU
    public bool IsTableau(GameObject pile)
    {
        foreach (GameObject tableau in this.tableaus)
            if (pile == tableau)
                return true;
        return false;
    }



    // CHECK IF A PILE IS A FOUNDATION
    public bool IsFoundation(GameObject pile)
    {
        foreach (GameObject foundation in this.foundations)
            if (pile == foundation)
                return true;
        return false;
    }

   

    // SELECT A CARD
    private void SelectCard(GameObject card)
    {
        this.selectedCard = card;
        card.GetComponent<SpriteRenderer>().color = Constants.COLOR_SELECTION;

        if(this.tipsUp)
        {
            this.AddTips();
        }
    }



    // DESELECT A CARD
    private void DeselectCard(GameObject card)
    {
        if(card == this.selectedCard)
        {
            this.selectedCard = null;
            card.GetComponent<SpriteRenderer>().color = card.GetComponent<CardManager>().GetDefaultColor();
            this.RemoveTips();
        }
    }



    // ADD TIPS
    private void AddTips()
    {
        if(this.selectedCard != null)
        {
            List<GameObject> piles = new List<GameObject>();
            piles.AddRange(this.tableaus);
            piles.AddRange(this.foundations);
            foreach (GameObject pile in piles)
            {
                if (this.IsValidMove(this.selectedCard, pile))
                {
                    GameObject tipCard = GameManager.GetTopCard(pile);
                    if (tipCard == null)
                    {
                        pile.GetComponent<SpriteRenderer>().color = Constants.COLOR_TIPS;
                        this.tips.Add(pile);
                    }
                    else
                    {
                        tipCard.GetComponent<SpriteRenderer>().color = Constants.COLOR_TIPS;
                        this.tips.Add(tipCard);
                    }
                }
            }
            this.AddScore(Constants.POINTS_HINTS);
        }
    }



    // REMOVE TIPS
    private void RemoveTips()
    {
        foreach (GameObject tip in this.tips)
        {
            if (tip.GetComponent<CardManager>() != null)
                tip.GetComponent<SpriteRenderer>().color = tip.GetComponent<CardManager>().GetDefaultColor();
            if (tip.GetComponent<PileManager>() != null)
                tip.GetComponent<SpriteRenderer>().color = tip.GetComponent<PileManager>().defaultColor;
        }
        this.tips.Clear();
    }



    // SWAP A SELECTION
    private void SelectDeselectCard(GameObject card)
    {
        if (this.selectedCard == null)
        {
            this.SelectCard(card);
        }
        else if (this.selectedCard == card)
        {
            this.DeselectCard(card);
        }
    }



    // DELAYED TEXT REMOVAL
    IEnumerator RemoveErrorText()
    {
        yield return new WaitForSeconds(Constants.TIME_DISAPPEAR);
        this.textErrorPortrait.enabled = this.textErrorLandscape.enabled = false;
    }



    // CHECK AND START VICTORY
    private void CheckIfVictory()
    {
        foreach (GameObject foundation in foundations)
            if (foundation.transform.childCount < Constants.VALUES.Length)
                return;
        this.StartVictoryAnimation();
    }



    // VICTORY
    private void StartVictoryAnimation()
    {
        this.particleHearts.SetActive(true);
        this.particleDiamonds.SetActive(true);
        this.particleSpades.SetActive(true);
        this.particleFlowers.SetActive(true);
        this.textVictoryPortrait.enabled = this.textVictoryLandscape.enabled = true;
        this.AddScore(Constants.POINTS_VICTORY * GameModel.GetDifficulty());
    }



    // ---------- BUTTONS ---------- //

    // UNDO LAST MOVE
    public void UndoMove()
    {
        if (this.lastMove == null)
        {
            this.textErrorPortrait.enabled = this.textErrorLandscape.enabled = true;
            StartCoroutine(this.RemoveErrorText());
        }
        else
        {
            if (this.IsFoundation(this.lastMove.targetPile))
            {                
                this.AddScore(0 - this.lastMove.cards[0].GetComponent<CardManager>().GetScore() + Constants.POINTS_BACK);
            }
            else
            {
                this.AddScore(Constants.POINTS_BACK);
            }
            foreach (GameObject card in this.lastMove.cards)
            {
                this.AddCardToPile(card, this.lastMove.originPile, true);
                if (this.lastMove.deckId > -1)
                {
                    card.transform.SetSiblingIndex(this.lastMove.deckId);
                }
            }

            this.lastMove = null;
        }

    }



    // TURN ON/OFF HINTS
    public void ToggleHints()
    {
        this.tipsUp = !this.tipsUp;
        if (this.tipsUp)
        {
            this.AddTips();
            ColorBlock theColor = this.hintsPortrait.colors;
            theColor.normalColor = Constants.COLOR_BUTTON_HINTS_ON;
            theColor.selectedColor = Constants.COLOR_BUTTON_HINTS_ON;
            this.hintsLandscape.colors = this.hintsPortrait.colors = theColor;
        }
        else
        {
            this.RemoveTips();
            ColorBlock theColor = this.hintsPortrait.colors;
            theColor.normalColor = Constants.COLOR_BUTTON_HINTS_OFF;
            theColor.selectedColor = Constants.COLOR_BUTTON_HINTS_OFF;
            this.hintsLandscape.colors = this.hintsPortrait.colors = theColor;
        }           
    }



    // TOGGLE PAUSE PANEL
    public void TurnPauseOnOff()
    {
        this.panelPausePortrait.SetActive(!this.panelPausePortrait.activeSelf);
        this.panelPauseLandscape.SetActive(!this.panelPauseLandscape.activeSelf);
    }



    // START THE GAME
    public void LoadMenu()
    {
        SceneManager.LoadScene(0);
    }



    // ---------- STATIC METHODS ---------- //

    // FIND AND REMOVE THE CARD WITH THE LOWEST Z IN THE LIST
    private static GameObject PullTopCard(List<GameObject> cardList)
    {
        GameObject topCard = GameManager.GetTopCard(cardList);
        cardList.Remove(topCard);
        return topCard;
    }



    // FIND THE CARD WITH THE LOWEST Z IN A PILE/LIST
    private static GameObject GetTopCard(GameObject pile)
    {
        List<GameObject> cardList = new List<GameObject>();
        foreach (Transform card in pile.transform)
            cardList.Add(card.gameObject);
        return GameManager.GetTopCard(cardList);
    }
    
    private static GameObject GetTopCard(List<GameObject> cardList)
    {
        GameObject topCard = null;
        foreach (GameObject card in cardList)
        {
            if (topCard == null || card.GetComponent<CardManager>().GetTargetPosition().z < topCard.GetComponent<CardManager>().GetTargetPosition().z)
                topCard = card;
        }
        return topCard;
    }



    // GET ALL FACE UP CARDS OVER THE TARGET ONE (ITSELF INCLUDED)
    private static List<GameObject> GetTopFlush(GameObject card)
    {
        List<GameObject> flush = new List<GameObject>();
        flush.Add(card);
        if(card.transform.parent != null)
            foreach(Transform cardToCheck in card.transform.parent)
                if (cardToCheck.GetComponent<CardManager>().GetTargetPosition().z < card.GetComponent<CardManager>().GetTargetPosition().z && cardToCheck.GetComponent<CardManager>().IsTurnedFaceUp())
                    flush.Add(cardToCheck.gameObject);
        return flush;
    }



    // FIND THE TOP CARD OF A PILE ONLY FACE UP OR ONLY FACE DOWN
    private static GameObject GetTopFaceUpCard(GameObject pile, bool hasFaceUp)
    {
        GameObject topCard = null;
        for (int i = 0; i < pile.transform.childCount; i++)
        {
            GameObject card = pile.transform.GetChild(i).gameObject;
            if (card.GetComponent<CardManager>().IsTurnedFaceUp() == hasFaceUp)
            {
                if (topCard == null)
                    topCard = card;
                else if (card.GetComponent<CardManager>().GetTargetPosition().z < topCard.GetComponent<CardManager>().GetTargetPosition().z)
                    topCard = card;
            }
        }
        return topCard;
    }



    // RETURN ALL CARDS
    public List<GameObject> GetAllCards()
    {
        List<GameObject> allCards = new List<GameObject>();
        foreach (Transform card in this.deck.transform)
            allCards.Add(card.gameObject);
        foreach(GameObject pile in this.tableaus)
            foreach (Transform card in pile.transform)
                allCards.Add(card.gameObject);
        foreach (GameObject pile in this.foundations)
            foreach (Transform card in pile.transform)
                allCards.Add(card.gameObject);
        return allCards;
    }
}
