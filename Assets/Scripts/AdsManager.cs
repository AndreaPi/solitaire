﻿using UnityEngine;
using UnityEngine.Advertisements;



public class AdsManager : MonoBehaviour, IUnityAdsListener
{
    private const string GAME_ID_GOOGLE_PLAY_STORE = "3722729";
    private const string AD_REWARDED_VIDEO = "rewardedVideo";



    private void Awake()
    {
        Advertisement.AddListener(this);
        Advertisement.Initialize(GAME_ID_GOOGLE_PLAY_STORE);
    }



    private void OnDestroy()
    {
        Advertisement.RemoveListener(this);
    }



    public void ShowAd()
    {
        if (Advertisement.IsReady(AdsManager.AD_REWARDED_VIDEO))
            Advertisement.Show(AdsManager.AD_REWARDED_VIDEO);
    }



    public void OnUnityAdsReady(string placementId){}
    public void OnUnityAdsDidError(string message){}
    public void OnUnityAdsDidStart(string placementId){}



    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        switch(showResult)
        {
            case ShowResult.Finished:
                this.GetComponent<MenuManager>().Play();
                break;
            case ShowResult.Failed:
                this.GetComponent<MenuManager>().Play();
                break;
            case ShowResult.Skipped:
                break;
        }
    }

}
