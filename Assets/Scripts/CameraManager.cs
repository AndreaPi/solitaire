﻿using UnityEngine;
using UnityEngine.UI;

public class CameraManager : MonoBehaviour
{
    public GameObject deck;
    public GameObject foundations;
    public GameObject tableaus;
    public GameObject UIPortrait;
    public GameObject UILandscape;
    public GameObject cameraPortrait;
    public GameObject cameraLandscape;



    private void Awake()
    {
        Screen.orientation = ScreenOrientation.AutoRotation;

        float unitsPerPixel;
        float desiredHalfHeight;

        unitsPerPixel = Constants.SCENE_WITDH_PORTRAIT / Screen.width;
        desiredHalfHeight = 0.5f * unitsPerPixel * Screen.height;
        this.cameraPortrait.GetComponent<Camera>().orthographicSize = desiredHalfHeight;

        unitsPerPixel = Constants.SCENE_WITDH_LANDSCAPE / Screen.width;
        desiredHalfHeight = 0.5f * unitsPerPixel * Screen.height;
        this.cameraLandscape.GetComponent<Camera>().orthographicSize = desiredHalfHeight;


        if (Screen.orientation == ScreenOrientation.Portrait || Screen.orientation == ScreenOrientation.PortraitUpsideDown)
        {
            this.cameraPortrait.SetActive(true);
            this.cameraLandscape.SetActive(false);
        }
        else
        {
            this.cameraPortrait.SetActive(false);
            this.cameraLandscape.SetActive(true);
        }
    }


    private void Update()
    {
        if(Screen.orientation == ScreenOrientation.Portrait || Screen.orientation == ScreenOrientation.PortraitUpsideDown)
        // if(Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown || Input.deviceOrientation == DeviceOrientation.Portrait)
        {
            if (!this.cameraPortrait.activeSelf)
            {
                this.RedesignPortrait();
            }
        }
        if (Screen.orientation == ScreenOrientation.Landscape || Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.LandscapeRight)
        {
            if(!this.cameraLandscape.activeSelf)
            {
                this.RedesignLandscape();
            }
        }

    }



    private void RedesignPortrait()
    {
        // UI
        this.UIPortrait.SetActive(true);
        this.UILandscape.SetActive(false);

        // CAMERA        
        this.cameraPortrait.SetActive(true);
        this.cameraLandscape.SetActive(false);

        // TABLE
        this.deck.transform.position = Constants.POSITION_DECK_PORTRAIT;
        this.foundations.transform.position = Constants.POSITION_FOUNDATIONS_PORTRAIT;
        this.tableaus.transform.position = Constants.POSITION_TABLEAUS_PORTRAIT;

        // CARDS
        foreach (GameObject card in this.GetComponent<GameManager>().GetAllCards())
            card.GetComponent<CardManager>().ConvertToOrientation(true);
    }



    private void RedesignLandscape()
    {
        // UI
        this.UIPortrait.SetActive(false);
        this.UILandscape.SetActive(true);

        // CAMERA        
        this.cameraPortrait.SetActive(false);
        this.cameraLandscape.SetActive(true);

        // TABLE
        this.deck.transform.position = Constants.POSITION_DECK_LANDSCAPE;
        this.foundations.transform.position = Constants.POSITION_FOUNDATIONS_LANDSCAPE;
        this.tableaus.transform.position = Constants.POSITION_TABLEAUS_LANDSCAPE;

        // CARDS
        foreach (GameObject card in this.GetComponent<GameManager>().GetAllCards())
            card.GetComponent<CardManager>().ConvertToOrientation(false);
    }
}
