﻿// REPRESENT TEMP DATAS ABOUT THE GAME RUNNING
public class GameModel
{
    private static Language language;
    private static int difficulty = 3;


    public static int GetDifficulty()
    {
        return GameModel.difficulty;
    }

    public static Language GetLanguage()
    {
        return GameModel.language;
    }

    public static void SetDifficulty(int difficulty)
    {
        GameModel.difficulty = difficulty;
    }

    public static void SetLanguage(Language language)
    {
        GameModel.language = language;
    }


}
