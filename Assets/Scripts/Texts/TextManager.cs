﻿
public static class TextManager
{
    // HOME
    public static string GetTextTitle()
    {
        switch (GameModel.GetLanguage())
        {
            case Language.italiano:
                return Italiano.TEXT_TITLE;
            case Language.english:
                return English.TEXT_TITLE;
            default:
                return English.TEXT_TITLE;
        }
    }

    public static string GetTextButtonHome()
    {
        switch (GameModel.GetLanguage())
        {
            case Language.italiano:
                return Italiano.TEXT_BUTTON_HOME;
            case Language.english:
                return English.TEXT_BUTTON_HOME;
            default:
                return English.TEXT_BUTTON_HOME;
        }
    }

    public static string GetTextButtonNewGame()
    {
        switch (GameModel.GetLanguage())
        {
            case Language.italiano:
                return Italiano.TEXT_BUTTON_NEWGAME;
            case Language.english:
                return English.TEXT_BUTTON_NEWGAME;
            default:
                return English.TEXT_BUTTON_NEWGAME;
        }
    }

    public static string GetTextButtonCredits()
    {
        switch (GameModel.GetLanguage())
        {
            case Language.italiano:
                return Italiano.TEXT_BUTTON_CREDITS;
            case Language.english:
                return English.TEXT_BUTTON_CREDITS;
            default:
                return English.TEXT_BUTTON_CREDITS;
        }
    }

    public static string GetTextButtonQuit()
    {
        switch (GameModel.GetLanguage())
        {
            case Language.italiano:
                return Italiano.TEXT_BUTTON_QUIT;
            case Language.english:
                return English.TEXT_BUTTON_QUIT;
            default:
                return English.TEXT_BUTTON_QUIT;
        }
    }

    public static string GetTextCredits()
    {
        switch (GameModel.GetLanguage())
        {
            case Language.italiano:
                return Italiano.TEXT_CREDITS;
            case Language.english:
                return English.TEXT_CREDITS;
            default:
                return English.TEXT_CREDITS;
        }
    }

    public static string GetTextDifficultyEasy()
    {
        switch (GameModel.GetLanguage())
        {
            case Language.italiano:
                return Italiano.TEXT_DIFFICULTY_EASY;
            case Language.english:
                return English.TEXT_DIFFICULTY_EASY;
            default:
                return English.TEXT_DIFFICULTY_EASY;
        }
    }

    public static string GetTextDifficultyMedium()
    {
        switch (GameModel.GetLanguage())
        {
            case Language.italiano:
                return Italiano.TEXT_DIFFICULTY_MEDIUM;
            case Language.english:
                return English.TEXT_DIFFICULTY_MEDIUM;
            default:
                return English.TEXT_DIFFICULTY_MEDIUM;
        }
    }

    public static string GetTextDifficultyHard()
    {
        switch (GameModel.GetLanguage())
        {
            case Language.italiano:
                return Italiano.TEXT_DIFFICULTY_HARD;
            case Language.english:
                return English.TEXT_DIFFICULTY_HARD;
            default:
                return English.TEXT_DIFFICULTY_HARD;
        }
    }



    // TABLE
    public static string GetTextError()
    {
        switch(GameModel.GetLanguage())
        {
            case Language.italiano:
                return Italiano.TEXT_ERROR;
            case Language.english:
                return English.TEXT_ERROR;
            default:
                return English.TEXT_ERROR;
        }
    }

    public static string GetTextPause()
    {
        switch (GameModel.GetLanguage())
        {
            case Language.italiano:
                return Italiano.TEXT_PAUSE;
            case Language.english:
                return English.TEXT_PAUSE;
            default:
                return English.TEXT_PAUSE;
        }
    }

    public static string GetTextScore()
    {
        switch (GameModel.GetLanguage())
        {
            case Language.italiano:
                return Italiano.TEXT_SCORE;
            case Language.english:
                return English.TEXT_SCORE;
            default:
                return English.TEXT_SCORE;
        }
    }

    public static string GetTextVictory()
    {
        switch (GameModel.GetLanguage())
        {
            case Language.italiano:
                return Italiano.TEXT_VICTORY;
            case Language.english:
                return English.TEXT_VICTORY;
            default:
                return English.TEXT_VICTORY;
        }
    }
}
