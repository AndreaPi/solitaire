﻿

public static class Italiano
{
    // HOME
    public const string TEXT_TITLE = "SOLITARIO";
    public const string TEXT_BUTTON_NEWGAME = "GIOCA";
    public const string TEXT_BUTTON_CREDITS = "CREDITI";
    public const string TEXT_BUTTON_QUIT = "ESCI";
    public const string TEXT_CREDITS = "Sviluppato da Andrea Picciolo";
    public const string TEXT_DIFFICULTY_EASY = "FACILE";
    public const string TEXT_DIFFICULTY_MEDIUM = "MEDIO";
    public const string TEXT_DIFFICULTY_HARD = "DIFFICILE";

    // TABLE
    public const string TEXT_BUTTON_HOME = "AL MENU";
    public const string TEXT_ERROR = "Non è possibile annullare questa mossa";
    public const string TEXT_PAUSE = "Gioco in pausa";
    public const string TEXT_SCORE = "Punteggio: ";
    public const string TEXT_VICTORY = "HAI VINTO!!!";
}
