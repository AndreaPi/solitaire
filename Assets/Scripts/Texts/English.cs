﻿

public static class English
{
    // HOME
    public const string TEXT_TITLE = "SOLITAIRE";
    public const string TEXT_BUTTON_NEWGAME = "PLAY";
    public const string TEXT_BUTTON_CREDITS = "CREDITS";
    public const string TEXT_BUTTON_QUIT = "QUIT";
    public const string TEXT_CREDITS = "Developed by Andrea Picciolo";
    public const string TEXT_DIFFICULTY_EASY = "EASY";
    public const string TEXT_DIFFICULTY_MEDIUM = "MEDIUM";
    public const string TEXT_DIFFICULTY_HARD = "HARD";

    // TABLE
    public const string TEXT_BUTTON_HOME = "TO HOME";
    public const string TEXT_ERROR = "Cannot undo this move";
    public const string TEXT_PAUSE = "Game paused";
    public const string TEXT_SCORE = "Score: ";
    public const string TEXT_VICTORY = "YOU WON!!!";
}
