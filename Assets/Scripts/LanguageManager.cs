﻿using UnityEngine;
using UnityEngine.UI;



public class LanguageManager : MonoBehaviour
{
    public MenuManager menuManager;
    private Dropdown languageDropDown;


    private void Awake()
    {
        this.languageDropDown = GetComponent<Dropdown>();
        this.languageDropDown.onValueChanged.AddListener(delegate {
            DropdownValueChanged(this.languageDropDown);
        });
    }



    void DropdownValueChanged(Dropdown change)
    {
        switch(change.value)
        {
            case 0:
                GameModel.SetLanguage(Language.italiano);
                break;
            case 1:
                GameModel.SetLanguage(Language.english);
                break;
        }
        this.menuManager.GetComponent<MenuManager>().UpdateTexts();
    }
}
