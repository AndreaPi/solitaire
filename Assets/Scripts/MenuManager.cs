﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public GameObject panelCredits;
    public Text textTitle;
    public Text textButtonNewGame;
    public Text textButtonCredits;
    public Text textButtonQuit;
    public Text textCredits;
    public Dropdown languages;
    public Dropdown difficulty;



    private void Awake()
    {
        this.UpdateTexts();
    }



    // START THE GAME
    public void Play()
    {
        GameModel.SetDifficulty(difficulty.value + 1);
        SceneManager.LoadScene(1);
    }



    // TOGGLE CREDITS PANEL
    public void TurnCreditsOnOff()
    {
        this.panelCredits.SetActive(!this.panelCredits.activeSelf);
    }



    // QUIT THE GAME
    public void QuitGame()
    {
        Application.Quit();
    }



    public void UpdateTexts()
    {
        this.textTitle.text = TextManager.GetTextTitle();
        this.textButtonNewGame.text = TextManager.GetTextButtonNewGame();
        this.textButtonCredits.text = TextManager.GetTextButtonCredits();
        this.textButtonQuit.text = TextManager.GetTextButtonQuit();
        this.textCredits.text = TextManager.GetTextCredits();
        this.languages.value = (int)GameModel.GetLanguage();

        this.difficulty.options[0].text = TextManager.GetTextDifficultyEasy();
        this.difficulty.options[1].text = TextManager.GetTextDifficultyMedium();
        this.difficulty.options[2].text = TextManager.GetTextDifficultyHard();
        this.difficulty.captionText.text = this.difficulty.options[this.difficulty.value].text;
    }
}
