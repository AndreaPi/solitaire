﻿using UnityEngine;



public class PileManager : MonoBehaviour
{
    public float offsetYFaceUp = -0.2f;
    public float offsetYFaceDown = -0.04f;
    public Color defaultColor;

    private void Awake()
    {
        this.defaultColor = this.GetComponent<SpriteRenderer>().color;
    }
}
