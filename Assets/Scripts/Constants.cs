﻿
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;

public static class Constants
{
    // CONSTANTS
    public static readonly char[] SEEDS = { 'H', 'D', 'S', 'F' };
    public static readonly char[] VALUES = { 'A', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'J', 'Q', 'K' };
    public const float OFFSET_Z = -0.01f;
    public const float TIME_DISAPPEAR = 5f;
    public const float TIME_PREPARE_DELAY = 0.1f;

    public const int POINTS_BACK = -5;
    public const int POINTS_CARD_MULTIPLIER = 3;
    public const int POINTS_DECK_RESHUFFLE = -10;
    public const int POINTS_HINTS = -1;
    public const int POINTS_VICTORY = 100;

    public static readonly Color COLOR_SELECTION = Color.cyan;
    public static readonly Color COLOR_TIPS = Color.yellow;
    public static readonly Color COLOR_BUTTON_HINTS_OFF = Color.black;
    public static readonly Color COLOR_BUTTON_HINTS_ON = Color.yellow;

    public const float SCENE_WITDH_PORTRAIT = 6;
    public const float SCENE_WITDH_LANDSCAPE = 3;

    public static readonly UnityEngine.Vector3 POSITION_DECK_PORTRAIT = new UnityEngine.Vector3(-2.4f, 4f, 0f);
    public static readonly UnityEngine.Vector3 POSITION_DECK_LANDSCAPE = new UnityEngine.Vector3(-2.5f, 2.3f, 0f);
    public static readonly UnityEngine.Vector3 POSITION_FOUNDATIONS_PORTRAIT = new UnityEngine.Vector3(0f, 4f, 0f);
    public static readonly UnityEngine.Vector3 POSITION_FOUNDATIONS_LANDSCAPE = new UnityEngine.Vector3(-2.5f, 0.8f, 0f);
    public static readonly UnityEngine.Vector3 POSITION_TABLEAUS_PORTRAIT = new UnityEngine.Vector3(0f, 2.5f, 0f);
    public static readonly UnityEngine.Vector3 POSITION_TABLEAUS_LANDSCAPE = new UnityEngine.Vector3(3.5f, 2.3f, 0f);



    // CHECK IF A SEDD IS RED OR BLACK
    public static bool IsRedSeed(char seed)
    {
        for(int i=0; i<Constants.SEEDS.Length/2; i++)
        {
            if (seed == Constants.SEEDS[i])
                return true;
        }
        return false;
    }



    // CHECK THE SCORE OF A VALUE
    public static int GetScore(char value)
    {
        return Constants.GetValue(value) * Constants.POINTS_CARD_MULTIPLIER;
    }



    // CONVERT VALUE TO NUMBER
    public static int GetValue(char value)
    {
        for (int i = 0; i < Constants.VALUES.Length; i++)
        {
            if (value == Constants.VALUES[i])
                return i + 1;
        }
        return 0;
    }



    // SHUFFLE ANY LIST
    public static List<T> Shuffle<T>(List<T> inputList)
    {
        List<T> list = new List<T>();
        foreach (T element in inputList)
        {
            list.Add(element);
        }
        List<T> shuffledList = new List<T>();
        System.Random random = new System.Random();

        while (list.Count > 0)
        {
            int n = UnityEngine.Random.Range(0, list.Count);
            shuffledList.Add(list[n]);
            list.RemoveAt(n);
        }

        return shuffledList;
    }
}


// ENUMS
public enum Language
{
    italiano,
    english
}
